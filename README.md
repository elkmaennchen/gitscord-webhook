# GitScord webhook

A middleman webhook between Gitlab and Discord, in order to have fancy messages.

<img src="screenshots/commits.png" width="30%"></img>
<img src="screenshots/note_issue.png" width="30%"></img>
<img src="screenshots/issue.png" width="30%"></img>

[Traducción en Castellano más abajo](#gitscord-webhook-castellano), [English version below](#gitscord-webhook-english)

# GitScord webhook (Français)

GitScord est un webhook intermédiaire entre Gitlab et Discord, pour avoir des messages plus agréable à lire que ceux fournit nativement par Gitlab.

## Fonctionnalités
GitScord utilise un maximum de possibilités de mise en forme de Discord pour rendre les messages assez complets et lisibles à la fois.

Fonctionnalités existantes :
 * support des commits, tags, issues, notes, merge requests
 * mentionne un utilisateur/rôle lors d'une merge request
 * multilingue
 * contrôle des aspects du bot dans Discord
 * 100% PHP, installation simple avec très peu de dépendances

Fonctionnalités en développement :
 * support des job, pipeline, wikipage
 * mode moins verbeux

## Installation
### Prérequis
GitScord est écrit entièrement en PHP. Comme le module *JSON* fait parti du PHPcore, le seul prérequis est d'avoir PHP >= 5.2.0 ou PHP >= 7, compilé avec le module *curl*.

Le serveur sur lequel est placé le script doit être accessible en HTTP(S) depuis votre Gitlab, et ce serveur doit pouvoir faire une requête HTTP(S) vers *discordapp.com*.

### Configuration
Dans un premier temps, créez un webhook dans les paramètres de votre serveur Discord (se référer à l'aide sur le site officiel de Discord si besoin).

L'étape suivante consiste à créé un fichier de configuration *gitscord_webhook_config.json*.
Vous pouvez vous aider des fichiers *gitscord_webhook_config.json.exemple.full* et *gitscord_webhook_config.json.exemple.minimal*, qui contiennent respectivement la liste exhaustive des paramètres et la liste des paramètres obligatoirement requis.
Définition des paramètres :
 * discord_url : adresse du webhook fourni par Discord
 * language : langue des messages envoyés par le bot, choisir entre en, es et fr
 * MR_manager : nom de l'utilisateur/rôle à mentionner lors d'une merge request
 * bot_name : nom du bot sur Discord (remplace le nom défini dans Discord)
 * bot_avatar : image de profil du bot sur Discord  (remplace l'image défini dans Discord), mettre une URL absolue

Il suffit ensuite de mettre le fichier PHP (*gitscord_webhook.php*, mais vous pouvez le renommer à votre convenance) dans un des dossiers servis par votre serveur HTTP(S), avec le fichier *gitscord_webhook_config.json* juste a côté (ne pas renommer, ou changer le nom dans le script PHP également).

Enfin dans Gitlab, menus "Settings" puis "Integrations" d'un projet, indiquer l'URL du fichier PHP et choisissez les événements à afficher dans Discord.

## Contributions
Pour aider à développer ce projet, vous pouvez notamment ouvrir des issues sur Gitlab pour :
 * les éventuels bugs que vous constatez à l'utilisation
 * les failles de sécurité que vous trouvez (avec le label *Security*)
 * des idées de nouvelles fonctionnalités (avec le label *Feature*)


# GitScord webhook (Castellano)

GitScord es un webhook intermediario entre Gitlab y Discord, para tener mensajes más agradable para leer que los mensajes enviados por Gitlab naturalmente.

## Funcionalidades
GitScord usa lo máximo posible los formatos de Discord para obtener mensajes a la vez completos y comprensible.

Funcionalidades actuales :
 * soporte de los commits, tags, issues, notes, merge requests
 * menciona un usuario/rol en una merge request
 * plurilingüe
 * dominio de los aspectos del bot en Discord
 * 100% PHP, instalación sencilla con muy poco de dependencias

Funcionalidades en desarrollo :
 * suporte de los job, pipeline, wikipage
 * modo menos verboso

## Instalación
### Prérequis
GitScord es escrito totalmente en PHP. Dado que el módulo *JSON* hace parte del PHPcore, el único prerrequisito es tener configurado PHP >= 5.2.0 o PHP >= 7, compilado con el módulo *curl*.

Le servidor que aloja el script tiene que ser accesible en HTTP(S) desde su Gitlab, y este servidor tiene que poder enviar solicitudes HTTP(S) hacia *discordapp.com*.

### Configuración
Para empezar, crea un webhook en los parámetros de su servidor Discord (leer la ayuda en la pagina oficial de Discord si es necesario).

Después, crea un archivo de configuración *gitscord_webhook_config.json*.
Ayudase de los archivos *gitscord_webhook_config.json.exemple.full* y *gitscord_webhook_config.json.exemple.minimal*, que presentan respectivamente la lista exhaustiva de los parámetros y la lista de los parámetros obligatorios.
Definición de los parámetros :
 * discord_url : localización del webhook dado por Discord
 * language : idioma de los mensajes enviados por el bot, elegir entre en, es y fr
 * MR_manager : nombre del usuario/rol que mencionar para una merge request
 * bot_name : nombre del bot en Discord (remplaza el nombre definido en Discord)
 * bot_avatar : imagen de perfil del bot en Discord  (remplaza la imagen definida en Discord), poner una URL absoluta.

Enseguida, solo poner el archivo PHP (*gitscord_webhook.php*, pero puede renombrar lo como quiere) en una de las carpetas de su servidor HTTP(S), con el archivo *gitscord_webhook_config.json* justo a su lado (no renombrar, o cambiar el nombre también en el script PHP).

Al fin en Gitlab, menús "Settings" luego "Integrations" de un proyecto, indicar la URL del archivo PHP y elige las noticias que enviar en Discord.

## Contribuciones
Para soportar el desarrollo de este proyecto, puede particularmente abrir issues en Gitlab para :
 * los bugs que encuentra usando GitScord
 * los fallos de seguridad que nota (con el label *Security*)
 * ideas de nuevas funcionalidades (con el label *Feature*)

# GitScord webhook (English)

GitScord is a middleman webhook between Gitlab and Discord, in order to have nicer to read messages than messages auto-generated by Gitlab.

## Features
GitScord use the whole formatting possibilities of Discord, to make the messages both complete and easily readable.

Existing features :
 * support commits, tags, issues, notes, merge requests
 * mention an user/role for a merge request
 * multilingual
 * control bot aspects in Discord
 * 100% PHP, straightforward installation with few dependencies

In development features :
 * support job, pipeline, wikipage
 * less verbose mode

## Installation
### Requirement
GitScord is entirely written in PHP. As the *JSON* module is in PHPcore, the only requirement is to have installed PHP >= 5.2.0 or PHP >= 7, compiled with the *curl* module.

The server on which is put the script have to be reachable in HTTP(S) from your Gitlab, and this server need to be able to make a HTTP(S) request to *discordapp.com*.

### Configuration
First, create a webhook in the parameters of your Discord server (please refer to the help page on official Discord website if needed).

Then create a configuration file *gitscord_webhook_config.json*.
You can find help in the example files *gitscord_webhook_config.json.exemple.full* and *gitscord_webhook_config.json.exemple.minimal*, which respectively contains the full parameters list and the compulsory parameters list.
Definition of the parameters :
 * discord_url : address of the Discord-provided webhook
 * language : language of messages sent by the bot, choose between en, es and fr
 * MR_manager : name of the user/role to be mentioned on a merge request
 * bot_name : name of the bot on Discord (override the name set in Discord)
 * bot_avatar : avatar image of the bot on Discord  (override the image set in Discord), put an absolute URL

Put the PHP file (*gitscord_webhook.php*, but you can rename it as you want) in a folder of your HTTP(S) server, with the file *gitscord_webhook_config.json* in the same folder (do not rename it, or change the name in the PHP script too).

Finally, on Gitlab in menus "Settings" and "Integrations" of a project, put theURL of the PHP file and choose the event you need to notified of in Discord.

## Contributions
To be involved in the development of this project, you can in particular open issues of Gitlab for :
 * the bugs you noticed using it
 * security problem you found (with the label *Security*)
 * ideas of new features (with the label *Feature*)

